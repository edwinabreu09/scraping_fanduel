
public class coinFlip {

	final static int[] arr1 = {0,1};
	final static int[] arr2 = {0,1,0};
	final static int[] arr3 = {0,1,1};
	final static int[] arr4 = {0,1,1,0};
	final static int[] arr5 = {0,1,1,1};
	final static int[] arr6 = {1,0};
	final static int[] arr7 = {1,1};
	final static int[] arr8 = {1,0,1,1};
	final static int[] arr9 = {1,1,1,0};
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Number of pairs: "+ numOfSidePairs(arr1) +" Max Number of pairs if one changed: "+ changeOneCoin(arr1));
		System.out.println("Number of pairs: "+ numOfSidePairs(arr2)+ " Max Number of pairs if one changed: "+ changeOneCoin(arr2));
		System.out.println("Number of pairs: "+ numOfSidePairs(arr3)+ " Max Number of pairs if one changed: "+ changeOneCoin(arr3));
		System.out.println("Number of pairs: "+ numOfSidePairs(arr4)+ " Max Number of pairs if one changed: "+ changeOneCoin(arr4));
		System.out.println("Number of pairs: "+ numOfSidePairs(arr5)+ " Max Number of pairs if one changed: "+ changeOneCoin(arr5));
		System.out.println("Number of pairs: "+ numOfSidePairs(arr6)+ " Max Number of pairs if one changed: "+ changeOneCoin(arr6));
		System.out.println("Number of pairs: "+ numOfSidePairs(arr7)+ " Max Number of pairs if one changed: "+ changeOneCoin(arr7));
		System.out.println("Number of pairs: "+ numOfSidePairs(arr8)+ " Max Number of pairs if one changed: "+ changeOneCoin(arr8));
		System.out.println("Number of pairs: "+ numOfSidePairs(arr9)+ " Max Number of pairs if one changed: "+ changeOneCoin(arr9));
	}
	
	public static int numOfSidePairs(int[] arr)
	{
		int coin1=0,coin2=0,pairs=0;
		
		for(int i = 0; i < arr.length-1; i++)
		{
			coin1=arr[i];
			coin2=arr[i+1];
			
			if(coin1==coin2)
			pairs++;
		}
		return pairs++;
	}
	
	public static int changeOneCoin(int[] x)
	{
		int [] changedArray;
		int biggestChange = 0;
		
		for(int j = 0; j < x.length; j++)
		{
			changedArray = x.clone();
			if(x[j] == 0)
			changedArray[j]=1;
			else
			changedArray[j]=0;
			
			if(biggestChange < numOfSidePairs(changedArray))
			biggestChange =numOfSidePairs(changedArray);	
		}
		
		return biggestChange;
		
	}

}
