package equilibrium;

public class equilibrium {

	final static int[] arr1 = {0};
	final static int[] arr2 = {0,1};
	final static int[] arr3= {0,1,0,1};
	final static int[] arr4 = {0,1,2,3};
	final static int[] arr5 = {0,1,2,3,4,5,6,7,8,9};
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Number of equilibriums: "+ getEquilibrium(arr1));
		System.out.println("Number of equilibriums: "+ getEquilibrium(arr2));
		System.out.println("Number of equilibriums: "+ getEquilibrium(arr3));
		System.out.println("Number of equilibriums: "+ getEquilibrium(arr4));
		System.out.println("Number of equilibriums: "+ getEquilibrium(arr5));
		
	}
	
	public static int getEquilibrium(int [] arr)
	{
		int leftsum=0, rightsum=0,numOfEquals=0;
		
		if(arr.length > 2)
		for(int i = 1; i < arr.length; i++)
		{
			
			leftsum+=arr[i-1];
			for(int j = i + 1; j < arr.length; j++)
			{
				rightsum = arr[j];
			}
			
			if(leftsum==rightsum)
			numOfEquals++;
		}
		
		return numOfEquals;
	}
	
	/*
	This one we have an array.  
	we want to return all numbers i where left sum = right sum.
	
	i=1
	
	i[0] = i[2]+ i[3] + i[4]
	
	
	*/

}
