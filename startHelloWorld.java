import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class startHelloWorld {

	public startHelloWorld() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("hello world");
		displayString();
		displayInt();
		displayInt2();
		displayInt3();
		
		int myNum = 5;
		float myFloatNum = 5.99f;
		char myLetter = 'D';
		boolean myBool = true;
		String myText = "Hello";
		
		System.out.println("Hello " + myNum + " " + myFloatNum+ " "  + myLetter+ " "  + myBool+ " "  + myText);
	}
	
	public static void displayString()
	{
		String name = "John";
		System.out.println(name);
	}
	
	public static void displayInt()
	{
		int myNum = 15;
		System.out.println(myNum);
	}
	
	public static void displayInt2()
	{
		int myNum;
		myNum = 15;
		System.out.println(myNum);
	}
	
	public static void displayInt3()
	{
		int myNum = 15;
		myNum = 20;  // myNum is now 20
		System.out.println(myNum);
	}
	
	public static void extractDataFromXML() throws XPathExpressionException 
	{
		StringBuilder concatenated = new StringBuilder();
		XPath xpath = XPathFactory.newInstance().newXPath();
		String expression = "/test/nodeA/nodeC/text()";
		InputSource inputSource = new InputSource("sample.xml");
		NodeList nodes;
			nodes = (NodeList) xpath.evaluate(expression, inputSource, XPathConstants.NODESET);
		for(int i = 0; i < nodes.getLength(); i++) 
		{
			concatenated.append(nodes.item(i).getTextContent());
		}
	}
}
