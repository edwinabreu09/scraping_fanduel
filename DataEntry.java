import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JRadioButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionEvent;

public class DataEntry extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DataEntry frame = new DataEntry();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DataEntry() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(300, 500, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("First Name:");
		lblNewLabel.setBounds(25, 53, 70, 16);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Last Name:");
		lblNewLabel_1.setBounds(25, 99, 58, 16);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Age:");
		lblNewLabel_2.setBounds(25, 145, 58, 13);
		contentPane.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("City:");
		lblNewLabel_3.setBounds(25, 184, 58, 13);
		contentPane.add(lblNewLabel_3);
		
		textField = new JTextField();
		textField.setBounds(105, 50, 96, 19);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(105, 96, 96, 19);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(105, 142, 96, 19);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setBounds(105, 181, 96, 19);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		JButton button = new JButton("GO");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean insert = true;
				
				/*
				if(textField.getText().isEmpty())
				{
					JOptionPane.showMessageDialog(null, "The first textbox is empty");
					
				}
				if (textField_1.getText().isEmpty())
				{
					JOptionPane.showMessageDialog(null, "The second textbox is empty");
				}
				if (textField_2.getText().isEmpty())
				{
					JOptionPane.showMessageDialog(null, "The third textbox is empty");
				}
				else
				{
					JOptionPane.showMessageDialog(null, "The last textbox is empty");
				}
				*/
				
				if(insert)
				{
					try
					{
						String url = "jdbc:sqlserver://localhost:1433;databasename=fanduel;integratedSecurity=true";
						Connection connection = DriverManager.getConnection(url);
						
						String sql = "INSERT INTO PERSON (fname, lname, age, city) VALUES (" + textField.getText() + ", " + textField_1.getText() + ", " + textField_2.getText()+ ", " + textField_3.getText() +")";
						
						Statement statement = connection.createStatement();
						int rows = statement.executeUpdate(sql);
						
						if (rows>0)
						{
							System.out.println("Row has been inserted.");
							
						}
					}catch (SQLException q)
					{
						System.out.println(q.toString());
					}
					
					
				}
				
				
			
			}
		});
		button.setBounds(250, 116, 85, 21);
		contentPane.add(button);
	}
}
