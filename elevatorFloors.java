package elevatorFloors;

import java.util.ArrayList;
import java.util.List;

public class elevatorFloors {
	
	final static int weightCapacity = 200;
	final static int peopleCapacity = 5;
	final static int[][] arr =  {{80, 2}};
	final static int[][] arr1 = {{80, 2}, {70, 1}};
	final static int[][] arr2 = {{80, 2}, {70, 1},{80, 4}};
	final static int[][] arr3 = {{80, 2}, {70, 1},{80, 4}, {20, 1}};
	final static int[][] arr4 = {{80, 2}, {70, 1},{80, 4}, {20, 1},{60, 1}};
	final static int[][] arr5 = {{80, 2}, {70, 1},{80, 4}, {20, 1},{60, 1},{60, 3}};

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Number of floors stopped at : "+returnNumOfFloors(arr));
		System.out.println("Number of floors stopped at : "+returnNumOfFloors(arr1));
		System.out.println("Number of floors stopped at : "+returnNumOfFloors(arr2));
		System.out.println("Number of floors stopped at : "+returnNumOfFloors(arr3));
		System.out.println("Number of floors stopped at : "+returnNumOfFloors(arr4));
		System.out.println("Number of floors stopped at : "+returnNumOfFloors(arr5));
	}
	
	public static int returnNumOfFloors(int [][] arr)
	{
		int weightHolder = 0, 
			peopleHolder = 0, 
			numOfFloors=0;
		
		List<Integer> list = new ArrayList<Integer> (); 

		for(int i = 0; i < arr.length; i++)
		{
			weightHolder+=arr[i][0];
			peopleHolder++;
			
			if(weightHolder < weightCapacity && peopleHolder < peopleCapacity)
			{
				if(!list.contains(arr[i][1]))
				list.add(arr[i][1]);
				
				if(i == arr.length-1)
				numOfFloors+=list.size()+1;
			}
			else
			{
				numOfFloors+=list.size()+1;
				list.clear();
				
				weightHolder=0;
				weightHolder+=arr[i][0];
				peopleHolder=0;
				peopleHolder++;
				if(!list.contains(arr[i][1]))
				list.add(arr[i][1]);
				
				if(i == arr.length-1)
				numOfFloors+=list.size()+1;
			}
			
		}
		return numOfFloors;
	}
}
